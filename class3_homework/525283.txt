Since start of this course, I learned a lot of new things, but the most important for me are how the git works. The upstream/origin repository difference, how commits work, that fast forward exists and rebase option.

I mostly enjoy your active way of teaching, questions given to students that need to answer and your active problem solving, when student asks about something, then you answer it and show it in front of us in real time.

About missing part, I am not sure, if it was done on purpose, but I feel like, if I did not know anything about git, I would be a bit lost during our first (and therefore consequent) lectures. So if it was not on purpose, I would explain the git a bit more from start of the course.
