First of all, I would like to say that I appreciate you coming to our faculty. 
We do not have a course specifically dedicated to git and I think it is missing.

Regarding the course itself, I really like the format. The seminars go straight
to the point. You try to cover all the necessary features without going too much
into detail which could overload some of the students.

So far we have mostly covered all the basics that I already knew. Personally, I 
am looking forward to the last three seminars which I find the most interesting.

One thing I might point out I slightly missed was covering the .git directory.
I know we peeked into it during one seminar but covering it more thoroughly
could prove beneficial to the students.

Anyways, thanks again for coming here :))
